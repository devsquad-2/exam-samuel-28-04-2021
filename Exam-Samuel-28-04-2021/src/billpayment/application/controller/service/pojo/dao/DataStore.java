package billpayment.application.controller.service.pojo.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import billpayment.application.controller.service.pojo.User;

public class DataStore {
	
	List<User> data = new ArrayList<>();
	
	{
		//sample user
		data.add(new User("someone@email.com", "p@ssword", LocalDate.of(2021, 4, 30) , BigDecimal.valueOf(80.0)));
	}

	public boolean checkUserPassword(String email, String password) {
		
		for(var u: data) {
			if (u.getEmail().equals(email) && u.getPassword().equals(password)) {
				return true;
			}
		}
		
		return false;
	}
	
	public LocalDate getDueDate(String email) {
		for(var u: data) {
			if(u.getEmail().equals(email)) {
				return u.getDueDate();
			}
		}
		return null;
	}
	
	public BigDecimal getAmount(String email) {
		for(var u: data) {
			if(u.getEmail().equals(email)) {
				return u.getAmount();
			}
		}
		return null;
	}

}
