package billpayment.application.controller.service.pojo;

import java.math.BigDecimal;
import java.time.LocalDate;

public class User {
	
	private String email;
	private String password;
	private LocalDate dueDate;
	private BigDecimal amount;
	
	public User(String email, String password, LocalDate dueDate, BigDecimal amount) {
		super();
		this.email = email;
		this.password = password;
		this.dueDate = dueDate;
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public LocalDate getDueDate() {
		return dueDate;
	}
	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
