package billpayment.application.controller;

import billpayment.application.controller.service.PaymentService;

public class PaymentController {
	
	private PaymentService service;
	
	{
		service = new PaymentService();
	}

	public PaymentService getService() {
		return service;
	}

}
