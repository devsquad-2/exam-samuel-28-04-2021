package billpayment.application;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import billpayment.application.controller.PaymentController;

//Problem statement: Verify the credentials of the users and return their bill due date and amount due.

public class BillPayment {

	public static void main(String[] args) {
		
		var in = new BufferedReader(new InputStreamReader(System.in));
		
		var con = new PaymentController();
		
		String email;
		String password;
		LocalDate date;
		
		try {
			// sample user: "someone@email.com", "p@ssword"
			
			System.out.println("Username: ");		
			email = in.readLine();
			System.out.println("Password: ");
			password = in.readLine();
			
			if (con.getService().getData().checkUserPassword(email, password)) {
				System.out.println("Successfully Login");
			} else {
				System.out.println("Sorry invalid Credentials");
				return;
			}
			
			System.out.println("Payment Due Date " + con.getService().getData().getDueDate(email));
			System.out.println("Total Payable Amount: " + con.getService().getData().getAmount(email));
			
			System.out.println("Enter date (YYYY-MM-DD): ");
			date = LocalDate.parse(in.readLine());
			
			if (date.isAfter(con.getService().getData().getDueDate(email))){
				System.out.println("Total Payable Amount + penalty: " + con.getService().getData().getAmount(email).multiply(BigDecimal.valueOf(1.05)));
			} else {
				System.out.println("No penalty.");
			}
		} catch (IOException|DateTimeParseException e) {
			e.printStackTrace();
		}
		
	}

}
